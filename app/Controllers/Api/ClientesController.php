<?php

namespace App\Controllers\Api;

use App\Models\ClienteModel;
use CodeIgniter\RESTful\ResourceController;

class ClientesController extends ResourceController
{
    public function __construct()
    {
        $this->setModel(new ClienteModel());
    }

    public function index()
    {
        if (!empty($this)) {
            return $this->respond($this->model->findAll(), 200);
        }

        return $this->respond([], 200);
    }
}
